## **GT Ticket**

**GT Ticket** là hệ thống hỗ trợ, tiếp nhận Yêu cầu, đáp ứng các Dịch vụ Kỹ thuật mà GT cung cấp

- Mỗi yêu cầu là một Ticket
- Mỗi Ticket được tạo ra bởi User và được thực hiện bởi Assignee.
- Assignee thực hiện công việc để đáp ứng các yêu cầu mô tả trong Ticket
- Ticket được đóng bởi User.

## **Hình thức tiếp cận**

1. Tạo Ticket trực tiếp 

    - Áp dụng cho các yêu cầu thường xuyên, đơn giản
    	- Cần xác định Sản phẩm, Dịch vụ và mô tả chi tiết Nội dung yêu cầu.
    - Trong trường hợp chưa rõ thông tin
        - Tham khảo trang wiki https://docs.gt.vng.vn
        - Trao đổi trực tiếp với member (TOM, PQA, Leaders) của GT để được hướng dẫn
    - Requestor tạo ticket trên trang https://gt.vng.vn/ticket

2. Tạo Ticket gián tiếp
    - Áp dụng cho các yêu cầu có tính phức tạp
        - Cần trao đổi trực tiếp với các member của GT (TOM, Leader) để làm rõ yêu cầu
        - Cần thống nhất các nội dung cần đáp ứng.
    - GT member tạo ticket trên trang https://gt.vng.vn/ticket và Requestor (GS) xác nhận nội dung Ticket qua email

3. GT xử lý Ticket trên trang https://redmine.gt.vng.vn/

## **Hướng dẫn tạo Ticket**

### **Tạo Ticket trực tiếp**

1. Requestor có yêu cầu cần khởi tạo ticket:
    - Requestor tiến hành khỏi tạo ticket trên: [gt.vng.vn/ticket](https://gt.vng.vn/ticket) 


2. Requestor sẽ khởi tạo ticket như sau:
    - Bấm vào Tạo yêu cầu hỗ trợ
        ![Hình 1: ](../images/ticket/0.png)

    - Chọn Sản phẩm 
        ![Hình 1: ](../images/ticket/1.png)

    - Chọn Dịch vụ
        ![Hình 1: ](../images/ticket/2.png)

    - Chọn Vấn đề hỗ trợ 
        ![Hình 1: ](../images/ticket/3.png)

    - Nhập Tiêu đề 
        ![Hình 1: ](../images/ticket/4.png)
  
    - Chọn/nhập domain người có liên quan đến Ticket (sẽ nhận được Email thông báo)
        ![Hình 1: ](../images/ticket/5.png)
      
    - Nhập Nội dung cần hỗ trợ
        ![Hình 1: ](../images/ticket/6.png)
        
   	- Đính kèm file nếu có nhu cầu
        ![Hình 1: ](../images/ticket/7.png)
    
    - Kiểm tra lại thông tin và bấm xác nhận để khởi tạo Ticket
        ![Hình 1: ](../images/ticket/8.png)
    
    - Email thông báo Ticket sẽ được gửi đến cho Requestor, Assignee và người có liên quan
        ![Hình 1: ](../images/ticket/9.png)


### **Tạo Ticket gián tiếp**

1. Requestor có yêu cầu cần khởi tạo ticket gián tiếp:
    - GT Member nhận yêu cầu từ Requestor qua email, chat,...
    - GT Member tiến hành khỏi tạo ticket trên: [gt.vng.vn/ticket](https://gt.vng.vn/ticket) 

2. GT Member sẽ khởi tạo ticket như sau:

    - Các bước như [Tạo ticket trực tiếp]() 

    - Thêm vào tùy chọn **Tạo dùm người khác**
        ![Hình 1: ](../images/ticket/10.png)	

3. Requestor xác nhận Ticket
    - Requestor nhận được Email -> kiểm tra nội dung -> bấm vào đây
        ![Hình 1: ](../images/ticket/11.png) 

    - Requestor xác nhận được Ticket:
        - Nhập ghi chú / lý do -> chọn Xác nhận hoặc Hủy
            ![Hình 1: ](../images/ticket/12.png) 